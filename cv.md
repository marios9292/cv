Mario Sanz Martínez
=====================
Desarrollador FrontEnd
-----------------------

_Valencia, 1992._      

Desarrollador, motivado y con ganas de aprender y mejorar mis habilidades constantemente.
Siempre me ha llamado la atención el mundo del desarrollo y después de estar trabajando como diseñador decidí cambiar el rumbo y centrarme tan solo en hacer crecer mis habilidades como programador.
Actualmente y tras un tiempo de esfuerzo y constancia, estoy trabajando como desarrollador tal y como quería, pero mi objetivo sigue siendo el aprender mientras trabajo para evolucionar cada día.

HABILIDADES
-----------------------
`Javascript` || `CSS` || `HTML` || `React` || `Angular` || `VUE` || `Testing` || `Node.js` || `Typescript` || `Indesign` || `Illustrator` ||  `Photoshop` || `Docker` || `Git`

EXPERIENCIA
-----------------------
 * **Desarrollador Front End, GFT** | Julio 2019 - Presente

     Estoy en GFT realizando proyectos en la industria Fintech, como Junior tambien he tenido cursos de Front end para desarrollar mis habilidades y crecer como programador  


 * **Programador Junior Devscola** | Octubre 2018 - Julio 2019

     Me dediqué a trabajar en proyectos que entraban en la comunidad de la Devscola, estando muy involucrado con sus metodologías y su forma de trabajar.

 * **Diseñador gráfico, RS Comunicación Gráfica** | Marzo 2016 - Octubre 2018

     Estuve como maquetador, diseñando catálogos y en ocasiones retocando color.
     En este caso entré como diseñador y acabé gestionando el equipo de diseño y retoque.

EDUCACIÓN
-----------------------
 * **Run Devscola, Devscola (Valencia Spain)** | 2018 - 2019

    Realicé un Bootcamp (Run) de 5 meses para adentrarme mas a fondo en el desarrollo Web, y a partir de entonces sigo muy involucrado aun con esta comunidad, aprendiendo y haciendo crecer mis habilidades como desarrollador Web, Tales como las metodologías Agile y TDD

 * **Diseño Gráfico, Barreira Arte y diseño (Valencia, España)** | Octubre 2018 - Julio 2019

     Durante mis estudios como diseñador aprendí sobre todo a realizar carteleria y maquetación, pero también tuvimos experiencia en el desarrollo Web, realizando una single pages con Css y Html.

 * **Arte Final, Barreira arte y diseño (Valencia, España)** | Marzo 2016 - Octubre 2018

     Durante mis estudios de arte finalista me dediqué a desarrollar mis habilidades como maquetador, aprendiendo las bases mas importantes de la impresión.



_____________________________________________________

     Contactame en mariosanz92@gmail.com
  